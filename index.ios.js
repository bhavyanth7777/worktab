/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    AsyncStorage,
    View,
    Text
} from 'react-native';
import App from "./src/App";
import store from './src/lib/Persistor';
import { persistStore } from 'redux-persist';


export default class worktab extends Component {
    constructor(){
        super();
        this.state = {rehydrated: false}
    }
    componentWillMount(){
        persistStore(store,{storage: AsyncStorage},()=> {
            console.log("rehydrated");
            this.setState({rehydrated:true});
        });
    }
    render() {
        if(this.state.rehydrated){
            return (
                <App store={store}/>
            )
        }
        else {
            return(
                <View>
                    <Text style={{marginTop:100, alignSelf:'center'}}>Setting up...</Text>
                </View>
            )
        }
    }
}


AppRegistry.registerComponent('worktab', () => worktab);
