/**
 * Created by bhavyanthkolli on 28/06/17.
 */
import React, {Component} from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import Modal from 'react-native-root-modal';
import {View, Platform, StyleSheet, BackHandler} from 'react-native';
import SideMenu from './SideMenu';
import {Actions} from 'react-native-router-flux';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class HeaderComp extends Component {
    constructor(props){
        super(props);
    }

    toHome = ()=> {
        Actions.home();
        this.props.toggleSideMenu();
    };

    toAddPunch = ()=> {
        Actions.addPunch();
        this.props.toggleSideMenu();
    };

    toProfile = ()=> {
        Actions.profile();
        this.props.toggleSideMenu();
    };

    toMySchedule = ()=> {
        Actions.mySchedule();
        this.props.toggleSideMenu();
    };

    takeBack = ()=> {
        if(this.props.backPage === "Home"){
            Actions.home();
        }
        else if(this.props.backPage === "MySchedule"){
            Actions.mySchedule();
        }
    };

    logOut = ()=> {
        if(Platform.OS === 'android'){
            BackHandler.exitApp();
            this.props.toggleSideMenu();
        }
        else {
            Actions.login();
            this.props.toggleSideMenu();
        }
    };

    render() {
        let hamburgerOpacity = 0;
        let backOpacity = 0;
        let hamburgerDisabled = true;
        let backDisabled = true;

        if(this.props.loggedInStatus){
            hamburgerOpacity = 1;
            hamburgerDisabled = false;
        }
        if (this.props.backVisible){
            backOpacity = 1;
            backDisabled = false;
        }
        return (
            <View>
            <Modal style={styles.modalStyle} visible={this.props.sideMenuVisible}>
                <SideMenu toggleSideMenu={this.props.toggleSideMenu} toHome={this.toHome} toAddPunch={this.toAddPunch}
                          toProfile={this.toProfile} toMySchedule={this.toMySchedule} logOut={this.logOut}/>
            </Modal>
                <Header>
                    <Left>
                        <Button transparent onPress={this.props.toggleSideMenu} title="sideMenu" style={{opacity:hamburgerOpacity}} disabled={hamburgerDisabled}>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body style={{marginLeft:100}}>
                        <Title>WorkTab</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={this.takeBack} title="backButton" style={{opacity:backOpacity}} disabled={backDisabled}>
                            <Icon name="arrow-back" style={{color:'white'}}/>
                        </Button>
                    </Right>
                </Header>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modalStyle: {
        ...Platform.select({
            ios: {
                top:0,
            },
            android: {
                top:40
            },
        }),
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'rgba(224,224,224,0.8)',
        position:'absolute'
    }
});

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        sideMenuVisible: state.sideMenuVisible,
        loggedInStatus: state.loggedIn,
        backVisible: state.backVisible,
        backPage: state.backPage,
    }
}, mapDispatchToProps)(HeaderComp);

//-------------------------------

