import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, TextInput, Alert} from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import {Icon} from 'native-base';
import {Actions} from 'react-native-router-flux';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class Login extends Component{
    constructor(props){
        super(props);
        this.state = {pinText: "Enter pin (4 digits)", pinDigits:0}
    }

    componentWillMount(){
        this.props.loggedIn(false);
        this.props.showBackButton(false);
    }

    numberClicked = (number)=> {
        if(this.state.pinText === "Enter pin (4 digits)"){
            this.setState({pinText:number, pinDigits:1});
        }
        else {
            let pinText = this.state.pinText;
            let pinDigits = this.state.pinDigits;
            if(pinDigits<4){
                pinText = pinText+number;
                pinDigits = pinDigits+1;
                this.setState({pinText:pinText, pinDigits:pinDigits});
            }
            if(pinDigits === 4){
                if(pinText === this.props.pin){
                    this.props.loggedIn(true);
                    Actions.home();
                }
                else {
                    Alert.alert(
                        "Wrong pin",
                        "Please check the pin"
                    );
                }
            }

        }
    };

    cancel = ()=> {
        this.setState({pinText:"Enter pin (4 digits)", pinDigits:0});
    };

    deleteLastClicked = ()=> {
        if(this.state.pinText === "Enter pin (4 digits)"){
            Alert.alert(
                "Please enter a pin first"
            );
        }
        else {
            let pinText = this.state.pinText;
            let pinDigits = this.state.pinDigits;
            if(pinDigits > 0 && pinDigits !== 1){
                pinText = pinText.split('');
                pinText.pop();
                pinText = pinText.join("");
                this.setState({pinText:pinText, pinDigits:pinDigits-1});
            }

            if(pinDigits > 0 && pinDigits === 1){
                this.setState({pinText:"Enter pin (4 digits)", pinDigits:0});
            }

        }
    };

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flexDirection:'row', justifyContent:'space-around',marginTop: 20}}>
                <Text style={styles.titleText}>{this.state.pinText}</Text>
                </View>
                <Grid style={{marginTop:20}}>
                    <Row style={{justifyContent:'space-around'}}>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("1")}>
                            <Text style={styles.cellItemText}>1</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("2")}>
                            <Text style={styles.cellItemText}>2</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("3")}>
                            <Text style={styles.cellItemText}>3</Text>
                        </TouchableOpacity>
                    </Row>
                    <Row style={{justifyContent:'space-around'}}>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("4")}>
                            <Text style={styles.cellItemText}>4</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("5")}>
                            <Text style={styles.cellItemText}>5</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("6")}>
                            <Text style={styles.cellItemText}>6</Text>
                        </TouchableOpacity>
                    </Row>
                    <Row style={{justifyContent:'space-around'}}>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("7")}>
                            <Text style={styles.cellItemText}>7</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("8")}>
                            <Text style={styles.cellItemText}>8</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("9")}>
                            <Text style={styles.cellItemText}>9</Text>
                        </TouchableOpacity>
                    </Row>
                    <Row style={{justifyContent:'space-around'}}>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.cancel()}>
                            <Text style={styles.cellItemText}>C</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.numberClicked("0")}>
                            <Text style={styles.cellItemText}>0</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.cellItem} onPress={()=>this.deleteLastClicked()}>
                            <Icon name='backspace' style={{color:'white'}}/>
                        </TouchableOpacity>
                    </Row>
                </Grid>
            </View>
        );
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        pin: state.pin,
    }
}, mapDispatchToProps)(Login);

//-------------------------------

const styles = StyleSheet.create({
    titleText:{
        fontSize:18,
        color:'#55BADF',
    },
    cellItem: {backgroundColor:'#55BADF',borderWidth:1,borderRadius:40, width:80, height:80, borderColor:'#55BADF', justifyContent:'center',alignItems:'center'},
    cellItemText: {color:'white', fontSize:20,fontWeight:'600'}

});