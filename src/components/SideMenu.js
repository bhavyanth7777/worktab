/**
 * Created by bhavyanthkolli on 17/07/17.
 */
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import { Container, Content, Button, Icon, List, ListItem} from 'native-base';
const  x = Dimensions.get('window').width;

export default class SideMenu extends Component {
    render(){
        return (
          <View style={{flex:1, justifyContent:'space-between',width:x*0.8,backgroundColor:'white'}}>
              <List style={{marginTop:20}}>
                  <ListItem>
              <TouchableOpacity style={{flex:1, flexDirection:'row', alignItems:'center'}} onPress={this.props.toHome}>
                  <Icon name='home' style={{fontSize:30,color:'#55BADF'}}/>
                  <Text style={{fontSize:20, marginLeft:10}}>Home</Text>
              </TouchableOpacity>
                  </ListItem>

                  <ListItem>
              <TouchableOpacity style={{flex:1, flexDirection:'row', alignItems:'center'}} onPress={this.props.toMySchedule}>
                  <Icon name='calendar' style={{fontSize:30,color:'#8528FF'}}/>
                  <Text style={{fontSize:20, marginLeft:10}}>My Schedule</Text>
              </TouchableOpacity>
                  </ListItem>

                  <ListItem>
              <TouchableOpacity style={{flex:1, flexDirection:'row', alignItems:'center'}} onPress={this.props.toProfile}>
                  <Icon name='contact' style={{fontSize:30,color:'#275CE8'}}/>
                  <Text style={{fontSize:20, marginLeft:10}}>Profile</Text>
              </TouchableOpacity>
                  </ListItem>

                  <ListItem>
              <TouchableOpacity style={{flex:1, flexDirection:'row', alignItems:'center'}} onPress={this.props.toAddPunch}>
                  <Icon name='time' style={{fontSize:30,color:'#DF8A78'}}/>
                  <Text style={{fontSize:20, marginLeft:10}}>Add Punch</Text>
              </TouchableOpacity>
                  </ListItem>

                  <ListItem>
              <TouchableOpacity style={{flex:1, flexDirection:'row', alignItems:'center'}} onPress={this.props.logOut}>
                  <Icon name='power' style={{fontSize:30,color:'#E8910C'}}/>
                  <Text style={{fontSize:20, marginLeft:10}}>Log Out</Text>
              </TouchableOpacity>
                  </ListItem>

              </List>
              <View style={{flexDirection:'row', justifyContent:'center'}}>
                  <Text style={{marginLeft:10}}>© Ntranga IT Sevices</Text>
              </View>
          </View>
        );
    }
}