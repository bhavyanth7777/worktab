import React, { Component } from 'react';
import { Icon } from 'native-base';
import {View, TouchableOpacity, Text, Dimensions, StyleSheet, NetInfo, BackHandler, Platform} from 'react-native';
import moment from 'moment'
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

const {height, width} = Dimensions.get('window');
class Home extends Component {
    constructor(props){
        super(props);
    }

    componentWillMount(){
        const dispatchConnected = isConnected => {
            this.props.connectionStatus(isConnected);
            if(isConnected){
                this.props.syncQueue();
            }
        };
        NetInfo.isConnected.fetch().then().done(() => {
            console.log("Done");
            NetInfo.isConnected.addEventListener('change', dispatchConnected);
        });
        this.props.getStartupData();
        this.props.showBackButton(false);
    }

    mySchedule = ()=> {
        this.props.loadingAction(true);
        let fromDate = moment().startOf('month').format("YYYY-MM-DD");
        let toDate = moment(fromDate).endOf('month').format('YYYY-MM-DD');
        this.props.mySchedule({fromDate:fromDate,toDate:toDate, redirect:true});
    };

    logOut = ()=> {
        if(Platform.OS === 'android'){
            BackHandler.exitApp();
        }
        else {
            Actions.login();
        }
    };

    render() {
        return (
            <View style={{padding:15}}>
                <Spinner visible={this.props.loading} textContent={"Loading..."} textStyle={{color: '#FFF'}} cancelable={false}/>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={styles.card} onPress={Actions.addPunch}>
                            <Icon name='time' style={{fontSize:80,color:'#DF8A78'}}/>
                            <Text style={styles.cardText}>Add Punch</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.card} onPress={this.mySchedule}>
                        <Icon name='calendar' style={{fontSize:80, color:'#8528FF'}}/>
                        <Text style={styles.cardText}>My Schedule</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={styles.card} onPress={Actions.profile}>
                        <Icon name='contact' style={{fontSize:80, color:'#275CE8'}}/>
                        <Text style={styles.cardText}>Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.card} onPress={this.logOut}>
                        <Icon name='power' style={{fontSize:80, color:'#E8910C'}}/>
                        <Text style={styles.cardText}>Log Out</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        sideMenuVisible: state.sideMenuVisible,
        loading: state.loading,
    }
}, mapDispatchToProps)(Home);

//-------------------------------

const styles = StyleSheet.create({
    card: {
        marginVertical: 5,
        marginHorizontal: 5,
        flex: 1,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: "#ccc",
        flexWrap: "wrap",
        backgroundColor: "#fff",
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 1.5,
        elevation: 3,
        justifyContent:'space-around',
        alignItems:'center',
        height: height*0.3
    },
    cardText: {fontSize:18, fontWeight:'600'},
});

