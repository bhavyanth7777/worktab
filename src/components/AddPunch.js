import React, { Component } from 'react';
import { Icon } from 'native-base';
import {View, TouchableOpacity, Text, Dimensions, StyleSheet, Alert} from 'react-native';
import moment from 'moment';
import Modal from 'react-native-root-modal';
import BreaksModal from './BreaksModal';
import Spinner from 'react-native-loading-spinner-overlay';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

const {height, width} = Dimensions.get('window');
class AddPunch extends Component {
    constructor(props){
        super(props);
        this.state = {breaksInfo:[], breaksModalVisible:false}
    }

    componentDidMount(){
        this.setBreaksInfo();
        this.props.showBackButton(true);
        this.props.takeBackAction("Home");
    }

    addPunch = (punch)=> {
        if(this.props.networkStatus){
            this.props.loadingAction(true);
            fetch('https://us-central1-worktab-2cde4.cloudfunctions.net/gettime')
                .then((response)=> {
                    return response.json();
                }).then((data)=> {
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        // fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyC8NEO4Rd_gisefWdhNzdfig7RV0xM8w74`)
                        //     .then((response)=> {
                        //         return response.json();
                        //     })
                        //     .then((data)=> {
                        //         console.log(`Data : ${data}`);
                        //     });
                        this.props.addPunch({currentPunch:punch, lastPunchTime:data.time, punchDate: data.date, lat:position.coords.latitude, lng:position.coords.longitude, network: true});
                        this.props.loadingAction(false);
                    },
                    (error) => {
                        this.props.loadingAction(false);
                        alert(JSON.stringify(error))
                    },
                    {timeout:10000});
            }).catch((err)=> {
                this.props.loadingAction(false);
                Alert.alert(
                    "Something's wrong!",
                    `${err}`
                );
            });
        }
        else {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    if(position.coords.latitude === this.props.latFromState && position.coords.longitude === this.props.lngFromState){
                        Alert.alert(
                            "Try again",
                            "Please move at least 10 metres or outside your building and try again"
                        );
                    }
                    else {
                        this.props.addPunch({currentPunch:punch, lastPunchTime:moment(position.timestamp).format("HH:mm:ss"), punchDate: moment(position.timestamp).format('DD-MM-YYYY'), lat:position.coords.latitude, lng:position.coords.longitude, network: false});
                    }

                },
                (error) => alert(JSON.stringify(error)),
                {timeout:10000});
        }
    };

    setBreaksInfo = ()=> {
        let defaultBreaks = this.props.breaksInfo;
        defaultBreaks.push("Custom Break","Day End");
        this.setState({breaksInfo:defaultBreaks});
    };

    showBreaksModal = ()=> {
        this.setState({breaksModalVisible:true});
    };

    hideBreaksModal = ()=> {
        this.setState({breaksModalVisible:false});
    };

    render() {
        let inPunchDisable=false, outPunchDisable=false, inOpacity=1, outOpacity=1;
        if(this.props.currentPunch === "In"){
            inPunchDisable = true;
            outPunchDisable = false;
            inOpacity = 0.3;
        }
        if((this.props.currentPunch).indexOf("Out") !== -1){
            outPunchDisable = true;
            inPunchDisable = false;
            outOpacity = 0.3;
        }
        return (
            <View style={{padding:15}}>
                <Spinner visible={this.props.loading} textContent={"Loading..."} textStyle={{color: '#FFF'}} cancelable={false}/>
                <Modal style={{top:0, right: 0, bottom: 0, left: 0, backgroundColor: 'rgba(224,224,224,0.8)'}} visible={this.state.breaksModalVisible}>
                    <BreaksModal hideBreaksModal={this.hideBreaksModal} addPunch={this.addPunch} breaksInfo={this.state.breaksInfo}/>
                </Modal>
                <View style={{alignSelf:'center', alignItems:'center'}}>
                    <Text style={styles.statusText}>{this.props.punchDate}</Text>
                    <Text style={[styles.statusTextBold,{marginTop:10}]}>{this.props.currentPunch + " - "+ this.props.lastPunchTime}</Text>
                </View>
                <View style={{flexDirection:'row', marginTop:10, opacity:inOpacity}} >
                    <TouchableOpacity style={styles.card} disabled={inPunchDisable} onPress={()=>this.addPunch("In")}>
                        <Icon name='log-in' style={{fontSize:80,color:'#00A839'}}/>
                        <Text style={styles.cardText}>In</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row', marginTop:10, opacity:outOpacity}}>
                    <TouchableOpacity style={styles.card} disabled={outPunchDisable} onPress={()=>this.showBreaksModal()}>
                        <Icon name='log-out' style={{fontSize:80, color:'#DF6D42'}}/>
                        <Text style={styles.cardText}>Out</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        currentPunch: state.punchData.currentPunch,
        lastPunchTime: state.punchData.lastPunchTime,
        networkStatus: state.networkStatus,
        latFromState: state.punchData.lat,
        lngFromState: state.punchData.lng,
        punchDate: state.punchData.punchDate,
        breaksInfo: state.startupData.breaksInfo,
        loading: state.loading,
    }
}, mapDispatchToProps)(AddPunch);

//-------------------------------

const styles = StyleSheet.create({
    card: {
        marginVertical: 5,
        marginHorizontal: 5,
        flex: 1,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: "#ccc",
        flexWrap: "wrap",
        backgroundColor: "#fff",
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 1.5,
        elevation: 3,
        justifyContent:'space-around',
        alignItems:'center',
        height: height*0.3
    },
    cardText: {fontSize:30, fontWeight:'600'},
    statusTextBold: {
        marginHorizontal:5,
        fontSize: 16,
        fontWeight:'600',
    },
    statusText: {
        marginHorizontal:5,
        fontSize: 15,
    }
});

