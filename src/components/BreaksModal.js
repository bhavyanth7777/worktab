/**
 * Created by bhavyanthkolli on 24/07/17.
 */
import React, {Component} from 'react';
import {List, ListItem, Button, Icon} from 'native-base';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
const  y = Dimensions.get('window').height;
const  x = Dimensions.get('window').width;


export default class BreaksModal extends Component {
    constructor(props){
        super(props);
    }

    selectBreak = (data)=> {
        this.props.addPunch(`Out(${data})`);
        this.props.hideBreaksModal();
    };
    render(){
        return(
            <View style={{marginTop:y*0.25,flex:1,width:null,backgroundColor:'white', paddingVertical:10}}>
                <Text style={{alignSelf:'center', color:'#55BADF'}}>Select Break Type</Text>
                <List style={{marginTop:20,flex:0.5}} dataArray={this.props.breaksInfo} renderRow={(data) =>
                    <ListItem>
                        <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>this.selectBreak(data)}>
                            <Text style={{fontSize:20}}>{data}</Text>
                        </TouchableOpacity>
                    </ListItem>
                }/>
                <Button iconRight full style={{backgroundColor:'#55BADF', alignSelf:'center', width:x, top:10, height:60, alignItems:'center'}} title="cancelBreak" onPress={this.props.hideBreaksModal}>
                    <Text style={{color:'white', fontSize:18}}>Cancel</Text>
                </Button>
            </View>
        );
    }
}