/**
 * Created by bhavyanthkolli on 01/08/17.
 */
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class Profile extends Component {

    componentDidMount(){
        this.props.showBackButton(true);
        this.props.takeBackAction("Home");
    }

    render(){
        return(
            <Container>
                <Content style={{marginTop:10}}>
                    <List>
                        <ListItem icon>
                            <Left>
                                <Icon name="person" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Employee Name</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.employeeName}</Text>
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Icon name="card" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Employee ID</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.employeeID}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon>
                            <Left>
                                <Icon name="mail" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Email ID</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.email}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon>
                            <Left>
                                <Icon name="call" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Phone Number</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.phone}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon>
                            <Left>
                                <Icon name="phone-portrait" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>IMEI</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.imei}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon>
                            <Left>
                                <Icon name="people" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Manager Name</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.managerName}</Text>
                            </Right>
                        </ListItem>


                        <ListItem icon>
                            <Left>
                                <Icon name="briefcase" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Company Name</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.companyName}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon>
                            <Left>
                                <Icon name="folder" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Department Name</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.departmentName}</Text>
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Icon name="clipboard" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Customer ID</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.customerID}</Text>
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Icon name="at" />
                            </Left>
                            <Body>
                            <Text style={styles.bodyText}>Company ID</Text>
                            </Body>
                            <Right>
                                <Text style={styles.rightText}>{this.props.userData.companyID}</Text>
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}

const styles = {
    bodyText: {
        fontSize:12,
        color:'grey'
    },
    rightText: {
        color:'black'
    }
};

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        userData: state.userData,
    }
}, mapDispatchToProps)(Profile);

//-------------------------------
