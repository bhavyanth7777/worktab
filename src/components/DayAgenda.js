/**
 * Created by bhavyanthkolli on 21/07/17.
 */
import React, {Component} from 'react';
import {Agenda} from 'react-native-calendars';
import {View, Text, StyleSheet} from 'react-native';
import moment from 'moment';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class DayAgenda extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.showBackButton(true);
        this.props.takeBackAction("MySchedule");
    }

    renderItem = (item) => {

        if(item["shiftID"]){
            return (
                <View style={[styles.item, {height: 120}]}>
                    <View>
                        <Text style={{fontSize: 15, fontWeight: '700'}}>Shift Info</Text>
                        <Text style={{marginTop: 5}}>Shift : {item["shiftID"]}</Text>
                        <Text>Shift Date : {item["shiftDate"]}</Text>
                        <Text>Start Time : {item["shiftStartTime"]}</Text>
                        <Text>End Time : {item["shiftEndTime"]}</Text>
                    </View>
                </View>
            );
        }

        if(item["currentPunch"]){
            let bgColor;
            if(item.currentPunch === "In"){
                bgColor = 'green';
            }
            else {
                bgColor = 'red';
            }
            return (
                <View style={styles.item}>
                    <Text style={{color:bgColor}}>{item.currentPunch + " - " + item.lastPunchTime}</Text>
                </View>
            );

        }

        if(item["shiftHours"]) {
            return (
                <View style={[styles.item, {height: 250}]}>
                    <View>
                        <Text style={{fontSize: 15, fontWeight: '700'}}>Consolidated Punches</Text>
                        <Text style={{marginTop: 5}}>Shift Hours : {item["shiftHours"]}</Text>
                        <Text>Worked Hours : {item["workedHours"]}</Text>
                        <Text>Balance Hours : {item["balanceHours"]}</Text>
                        <Text>Extra Hours : {item["extraHours"]}</Text>
                        <Text>Break Hours : {item["breakHours"]}</Text>
                        <Text>Over time Hours : {item["overTimeHours"]}</Text>
                        <Text>Late in Hours : {item["lateInHours"]}</Text>
                        <Text>Late out Hours : {item["lateOutHours"]}</Text>
                        <Text>Early in Hours : {item["earlyInHours"]}</Text>
                        <Text>Early Out Hours : {item["earlyOutHours"]}</Text>
                    </View>
                </View>
            );
        }

        if (item["punchType"]){
            let bgColor;
            if(item["punchType"] === "In"){
                bgColor = 'green';
            }
            else {
                bgColor = 'red';
            }
            return (
                <View style={styles.item}>
                    <Text style={{color:bgColor}}>{item["punchType"] + " - " + item["punchTimeStamp"]}</Text>
                    <Text style={{color:bgColor}}>{item["punchDate"]}</Text>
                </View>
            );
        }
    };

    renderEmptyDate =() => {
        return (
            <View style={styles.emptyDate}><Text>No punches</Text></View>
        );
    };

    rowHasChanged = (r1, r2)=> {
        return r1.name !== r2.name;
    };

    render(){
        return(
        <Agenda
            items={this.props.items}
            selected={this.props.selectedDate}
            renderItem={this.renderItem}
            renderEmptyDate={this.renderEmptyDate}
            rowHasChanged={this.rowHasChanged}
            enableEmptySections={true}
            onDayPress={(day)=>{this.props.getPunchData({dateString:day.dateString, redirect:false})}}
            // style={{marginTop:200}}
        />
        )
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        employeeShiftList: state.employeeShiftList,
        punchArchive: state.punchData.punchArchive,
        items: state.items.items,
        selectedDate: state.items.selectedDate,

    }
}, mapDispatchToProps)(DayAgenda);

//-------------------------------

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17,
        height:50
    },
    emptyDate: {
        height: 15,
        flex:1,
        paddingTop: 30
    }
});