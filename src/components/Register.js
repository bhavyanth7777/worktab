import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';
import { Container, Content, Form, Item, Input, Label, Icon, Button } from 'native-base';
import DeviceInfo from 'react-native-device-info';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class Register extends Component{
    constructor(props){
        super(props);
        this.state = {companyName:"", employeeId:"", phone:"", imei:"", userAgent:""}
    }
    componentDidMount(){
        let imei = DeviceInfo.getUniqueID();
        let userAgent = DeviceInfo.getUserAgent();
        this.setState({imei:"911481350009478", userAgent:userAgent});
    }

    register = ()=> {
        this.props.registerEmployee(this.state);
    };

    render(){
        return(
            <Container>
                <Content>
                    <Text style={styles.titleText}>Registration</Text>
                    <Form>
                        <Item floatingLabel>
                            <Icon active name='briefcase' style={{marginTop:5}}/>
                            <Label>Company Name</Label>
                            <Input style={{paddingLeft:10}} onChangeText={(text) => this.setState({companyName:text})}/>
                        </Item>
                        <Item floatingLabel>
                            <Icon active name='man' style={{marginTop:5}}/>
                            <Label>Employee ID</Label>
                            <Input style={{paddingLeft:10}} onChangeText={(text) => this.setState({employeeId:text})}/>
                        </Item>
                        <Item floatingLabel>
                            <Icon active name='call' style={{marginTop:5}}/>
                            <Label>Phone</Label>
                            <Input style={{paddingLeft:10}} onChangeText={(text) => this.setState({phone:text})}/>
                        </Item>
                        <Item floatingLabel>
                            <Icon active name='phone-portrait' style={{marginTop:5}}/>
                            <Label>IMEI</Label>
                            <Input disabled value={this.state.imei} style={{paddingLeft:10}} autoFocus={true}/>

                        </Item>
                    </Form>
                    <Button iconRight style={{alignSelf:'center', marginTop:20,backgroundColor:'#55BADF'}} onPress={this.register} title="Register">
                        <Text style={{color:'white'}}>Register</Text>
                        <Icon name='arrow-forward' />
                    </Button>
                </Content>
            </Container>
        )
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {

    }
}, mapDispatchToProps)(Register);

//-------------------------------

const styles = StyleSheet.create({
   titleText:{
       fontSize:18,
       color:'#55BADF',
       textAlign:'center',
       marginTop: 20,
   },
});