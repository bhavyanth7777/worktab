import React, {Component} from 'react';
import {View, Text} from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import moment from 'moment'
import {Actions} from 'react-native-router-flux';
import Spinner from 'react-native-loading-spinner-overlay';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class MySchedule extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.showBackButton(true);
        this.props.takeBackAction("Home");
    }

    mySchedule = (date)=> {
        let fromDate = moment(date).startOf('month').format("YYYY-MM-DD");
        let toDate = moment(date).endOf('month').format('YYYY-MM-DD');
        this.props.mySchedule({fromDate:fromDate,toDate:toDate,redirect:false});
    };

    selectDay = (day)=> {
        this.props.loadingAction(true);
        let dateString = day.dateString;
        this.props.getPunchData({dateString:dateString, redirect:true});
    };

    render(){
        let employeeShiftList = this.props.employeeShiftList;
        let markedDatesData = {};
        for (let i=0;i<employeeShiftList.length;i++){
            let shiftDate = employeeShiftList[i].shiftDate;
            let shiftID = employeeShiftList[i].shiftID;
            if(shiftID === "GENERAL"){
                if(employeeShiftList[i].leaveStatus && employeeShiftList[i].leaveStatus === "APR"){
                    if(shiftDate === moment().format('YYYY-MM-DD')){
                        markedDatesData[shiftDate] = [{startingDay: true, color: '#BA5A1C', textColor:'white'}, {endingDay: true, color: '#BA5A1C',textColor:'black'}]
                    }
                    else {
                        markedDatesData[shiftDate] = [{startingDay: true, color: '#BA5A1C', textColor:'white'}, {endingDay: true, color: '#BA5A1C',textColor:'white'}]
                    }

                }
                else {
                    if(shiftDate === moment().format('YYYY-MM-DD')){
                        markedDatesData[shiftDate] = [{startingDay: true, color: '#6EAB3A', textColor:'white'}, {endingDay: true, color: '#6EAB3A',textColor:'black'}]
                    }
                    else {
                        markedDatesData[shiftDate] = [{startingDay: true, color: '#6EAB3A', textColor:'white'}, {endingDay: true, color: '#6EAB3A',textColor:'white'}]
                    }

                }

            }
            else if (shiftID === "OFF"){
                markedDatesData[shiftDate] = [{startingDay: true, color: '#D3D3D3', textColor:'white'}, {endingDay: true, color: '#D3D3D3',textColor:'white'}]
            }
        }
        //6EAB3A - for general
        //BA5A1C - for off
        return (
            <View style={{flex:1, marginTop:20}}>
                <Spinner visible={this.props.loading} textContent={"Loading..."} textStyle={{color: '#FFF'}} cancelable={false}/>
                <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                    <View style={{flexDirection:'row'}}>
                        <View style={{height:20, width:20, borderWidth:1, borderRadius:10,borderColor:'#6EAB3A', backgroundColor:'#6EAB3A'}}>
                        </View>
                        <Text style={{marginLeft:3}}>General</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{height:20, width:20, borderWidth:1, borderRadius:10,borderColor:'#BA5A1C', backgroundColor:'#BA5A1C'}}>
                        </View>
                        <Text style={{marginLeft:3}}>Leave</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{height:20, width:20, borderWidth:1, borderRadius:10,borderColor:'#D3D3D3', backgroundColor:'#D3D3D3'}}>
                        </View>
                        <Text style={{marginLeft:3}}>Off</Text>
                    </View>
                </View>
                <Calendar
                    markedDates={markedDatesData}
                    onMonthChange={(month) => {this.mySchedule(month.dateString)}}
                    onDayPress={(day) => {this.selectDay(day)}}
                    markingType={'interactive'}
                    style={{
                        marginTop:20
                    }}
                />
            </View>
        );
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        employeeShiftList: state.employeeShiftList,
        punchArchive: state.punchData.punchArchive,
        loading: state.loading,

    }
}, mapDispatchToProps)(MySchedule);

//-------------------------------