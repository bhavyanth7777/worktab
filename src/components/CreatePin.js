import React, {Component} from 'react';
import {Text, StyleSheet, View, Alert} from 'react-native';
import { Container, Content, Form, Item, Input, Label, Icon, Button } from 'native-base';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class CreatePin extends Component {
    constructor(props){
        super(props);
        this.state = {pin:"", confirmPin:"",hidePin:true}
    }

    toggleVisibility = ()=> {
        if(this.state.hidePin){
            this.setState({hidePin:false})
        }
        else {
            this.setState({hidePin:true})
        }

    };

    createPin = ()=> {
        if((this.state.pin).length>0 && (this.state.confirmPin).length>0){
            if(this.state.pin === this.state.confirmPin){
                this.props.createPin(this.state.pin);
            }
            else {
                Alert.alert(
                    "Passwords don't match"
                );
            }
        }

        else {
            Alert.alert(
                "Oops!",
                "Fields cannot be empty"
            );
        }


    };



    render(){
        return(
            <Container>
                <Content>
                    <Text style={styles.titleText}>Create Pin</Text>
                    <Form>
                        <Item floatingLabel>
                            <Icon active name='lock' style={{marginTop:5}}/>
                            <Label>Enter Pin</Label>
                            <Input style={{paddingLeft:10}} secureTextEntry={this.state.hidePin} keyboardType="numeric"
                                   onChangeText={(text)=>this.setState({pin:text})} maxLength={4}/>
                        </Item>
                        <Item floatingLabel>
                            <Icon active name='lock' style={{marginTop:5}}/>
                            <Label>Confirm Pin</Label>
                            <Input style={{paddingLeft:10}} secureTextEntry={this.state.hidePin} keyboardType="numeric"
                                   onChangeText={(text)=>this.setState({confirmPin:text})} maxLength={4}/>
                        </Item>
                    </Form>
                    <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:20,paddingHorizontal:20}}>
                        <Button bordered warning onPress={this.toggleVisibility} title="toggleVisibility">
                            <Icon name='eye' />
                        </Button>
                    <Button iconRight style={{backgroundColor:'#55BADF'}} onPress={this.createPin} title="createPin">
                        <Text style={{color:'white'}}>Next</Text>
                        <Icon name='arrow-forward' />
                    </Button>
                    </View>
                </Content>
            </Container>

        );
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {

    }
}, mapDispatchToProps)(CreatePin);

//-------------------------------

const styles = StyleSheet.create({
    titleText:{
        fontSize:18,
        color:'#55BADF',
        textAlign:'center',
        marginTop: 20,
    },
});