import React, { Component } from 'react';
import { Router, Scene, ActionConst} from 'react-native-router-flux';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { Container, Content, Text, StyleProvider } from 'native-base';
import {Actions} from 'react-native-router-flux';


import { Provider } from 'react-redux';
import HeaderComp from './components/HeaderComp';
import Register from './components/Register';
import CreatePin from './components/CreatePin';
import Login from './components/Login';
import Home from './components/Home';
import AddPunch from './components/AddPunch';
import MySchedule from './components/MySchedule';
import DayAgenda from './components/DayAgenda';
import BreaksModal from './components/BreaksModal';
import Profile from './components/Profile';

//-------------------------------
import { connect } from 'react-redux';
import { ActionCreators } from './actions';
import {bindActionCreators} from 'redux';
//-------------------------------

class App extends Component {
    constructor(props){
        super(props);
        this.state = {register:true, login: false}
    }
    componentWillMount(){
        if(this.props.registered){
            this.setState({register:false, login:true});
        }
    }
    render() {
        return (
            <Provider store={this.props.store}>
                <StyleProvider style={getTheme(material)}>
                <Container>
                    <HeaderComp/>
                <Router hideNavBar={true}>
                    <Scene key="root">
                        <Scene key="register" component={Register} initial={this.state.register} type={ActionConst.RESET}/>
                        <Scene key="createPin" component={CreatePin}/>
                        <Scene key="home" component={Home}/>
                        <Scene key="mySchedule" component={MySchedule}/>
                        <Scene key="login" component={Login} initial={this.state.login}/>
                        <Scene key="addPunch" component={AddPunch} />
                        <Scene key="dayAgenda" component={DayAgenda}/>
                        <Scene key="profile" component={Profile}/>
                    </Scene>
                </Router>
                </Container>
                </StyleProvider>
            </Provider>
        );
    }
}

//-------------------------------

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state)=> {
    return {
        registered: state.registered,
        sideMenuVisible: state.sideMenuVisible,
    }
}, mapDispatchToProps)(App);

//-------------------------------