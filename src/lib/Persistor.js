import { createStore, applyMiddleware, compose } from 'redux';
import { autoRehydrate } from 'redux-persist';
import thunkMiddleWare from 'redux-thunk';
import { createLogger } from 'redux-logger';

import reducer from '../reducers';
//-----------------------------------------------------

const loggerMiddleWare = createLogger({ predicate: (getState, action) => __DEV__});

function configureStore(initialState) {
    const enhancer = compose(
        applyMiddleware(
            thunkMiddleWare,
        ),
        autoRehydrate()
    );
    return createStore(reducer, initialState, enhancer);
}

export default store = configureStore({});
