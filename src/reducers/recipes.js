import createReducer from '../lib/createReducer';
import * as types from '../actions/types';

export const registered = createReducer(false,{
    [types.REGISTERED](state,action){
        return action.payload;
    }

});

export const userData = createReducer({companyName:"", customerID:"", companyID:"", departmentName:"",
    employeeName:"",email:"",managerName:"", phone:"", employeeID:"", imei:""},{
    [types.STORE_USER_DATA](state,action){
        let {companyName, customerID, companyID, departmentName, employeeName,email,managerName,phone,employeeID,imei} = action.payload;
        return {...state, companyName:companyName, customerID:customerID, companyID:companyID, departmentName:departmentName,
            employeeName:employeeName,email:email,managerName:managerName,phone:phone,employeeID:employeeID,imei:imei}
    }
});

export const pin = createReducer("",{
   [types.STORE_PIN](state, action){
       return action.payload;
   }
});

export const loading = createReducer(false, {
    [types.LOADING](state, action){
        return action.payload;
    }
});

export const backVisible = createReducer(false, {
    [types.SHOW_BACK](state, action){
        return action.payload;
    }
});

export const backPage = createReducer("", {
    [types.TAKE_BACK](state, action){
        return action.payload;
    }
});

export const sideMenuVisible = createReducer(false, {
    [types.TOGGLE_SIDEMENU](state, action){
        return !state;
    }
});

export const startupData = createReducer({gpsInterval:"", appDateTime:"", shiftInfo:{}, breaksInfo:[]},{
    [types.STORE_STARTUP_DATA](state, action){
        let {gpsInterval, appDateTime, shiftInfo, breaksInfo} = action.payload;
        return {...state,gpsInterval:gpsInterval, appDateTime:appDateTime, shiftInfo:shiftInfo,breaksInfo:breaksInfo}
    }
});

export const employeeShiftList = createReducer([],{
    [types.STORE_SHIFT_DATA](state,action){
        return action.payload;
    }
});

export const punchData = createReducer({currentPunch:"NA",lastPunchTime:"NA",punchDate:"",lat:0,lng:0, punchQueue:[], punchArchive:{Date:"",Punches:[]}},{
    [types.ADD_PUNCH](state, action){
        return {...state, currentPunch:action.payload.currentPunch, lastPunchTime:action.payload.lastPunchTime, punchDate:action.payload.punchDate,lat:action.payload.lat, lng:action.payload.lng, punchArchive:action.payload.punchArchive}
    },
    [types.PUSH_TO_QUEUE](state, action){
        return {...state, punchQueue:action.payload};
    },
    [types.RESET_QUEUE](state, action){
        return {...state, punchQueue:[]}
    },
});


export const loggedIn = createReducer(false, {
    [types.LOGGED_IN](state, action){
        return action.payload;
    }
});

export const networkStatus = createReducer(false, {
   [types.CONNECTION_STATUS](state, action){
       return action.payload;
   }
});

export const items = createReducer({items:{}, selectedDate:""}, {
   [types.RENDER_ITEMS](state,action){
       let {items, selectedDate} = action.payload;
       return {items:items,selectedDate:selectedDate}
   }
});