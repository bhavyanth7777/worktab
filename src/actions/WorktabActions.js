import * as types from './types';
import {Alert} from 'react-native';
import {Actions} from 'react-native-router-flux';
import moment from 'moment';


export const createPin = (data)=> ((dispatch)=> {
   dispatch({
       type:types.STORE_PIN,
       payload:data
   });
   Actions.home();
});

export const loadingAction = (status) => ((dispatch)=> {
    dispatch({
        type: types.LOADING,
        payload: status
    });
});

export const getPunchData = (data) => ((dispatch, getState)=> {
    let day = data.dateString;
    let redirect = data.redirect;
   const state = getState();
   // let items = state.items.items;
    let items = {};
   let userData = state.userData;
   let shiftData = state.employeeShiftList;
   // check if day is today
    if(moment().format('YYYY-MM-DD') === day){
        // today's data to be added to items
        let punchArchive = state.punchData.punchArchive;
        let dateToShow = moment(punchArchive['Date'], 'DD-MM-YYYY');
        dateToShow = dateToShow.format('YYYY-MM-DD');

        let obj = shiftData.find(x => x["shiftDate"] === day);
        let index = shiftData.indexOf(obj);
        let punchArchiveList = punchArchive['Punches'];

        punchArchiveList.push(shiftData[index]);
        items[dateToShow] = punchArchiveList;



        dispatch({
            type: types.RENDER_ITEMS,
            payload: {items:items, selectedDate:day}
        });
        dispatch({
            type: types.LOADING,
            payload: false
        });
        if(redirect){
            Actions.dayAgenda();
        }
    }
    else {
        fetch('https://worktabapp.fulchrum.com/punch/punchdata', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                customerID:userData.customerID,
                companyID:userData.companyID,
                employeeID:userData.employeeID,
                deviceDetails:userData.imei,
                phoneNumber:userData.phone,
                selectedDate:day
            })
        }).then((response)=> {
            return response.json();
        }).then((data)=> {
            let message = data.message;
            if(message.messageType === "message") {
                let punches = data["punches"];
                let processedPunches = punches["processed"];
                let consolidatedPunches = data["consolidatedPunches"];
                let obj = shiftData.find(x => x["shiftDate"] === day);
                let index = shiftData.indexOf(obj);
                processedPunches.push(consolidatedPunches,shiftData[index]);
                items[day] = processedPunches;

                dispatch({
                    type: types.RENDER_ITEMS,
                    payload: {items:items, selectedDate:day}
                });
                dispatch({
                    type: types.LOADING,
                    payload: false
                });

                if(redirect){
                    Actions.dayAgenda();
                }

            }
            else {
                Alert.alert(
                    "Something's wrong!",
                    "Please try again in sometime"
                );
                dispatch({
                    type: types.LOADING,
                    payload: false
                });
            }

        }).catch((err)=> {
            dispatch({
                type: types.LOADING,
                payload: false
            });
            console.log(err);
            Alert.alert(
                "Something's wrong!",
                `${err}`
            );
        });
    }

});

export const addPunch = (data)=> ((dispatch, getState)=> {
   const state = getState();
   let userData = state.userData;
   let punchArchive = state.punchData.punchArchive;
   let {currentPunch, lastPunchTime, punchDate, lat, lng, network} = data;
   if(network){
       fetch('https://worktabapp.fulchrum.com/punch/addpunches', {
           method: 'POST',
           headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json',
           },
           body: JSON.stringify([{
               customerID:userData.customerID,
               companyID:userData.companyID,
               employeeID:userData.employeeID,
               punchType: currentPunch,
               punchDate:punchDate,
               punchTimeStamp: lastPunchTime,
               deviceDetails:userData.imei,
               phoneNumber:userData.phone,
               gpsLongitude: lng,
               gpsLatitude: lat,
               gpsLocation: ""
           }])
       }).then((response)=> {
           return response.json();
       }).then((data)=> {
           let message = data.message;
           if(message.messageType === "message"){
               let recentPunchDate = punchArchive["Date"];
               let recentPunchList = punchArchive["Punches"];
               if(recentPunchDate !== punchDate){
                   punchArchive["Date"] = punchDate;
                   recentPunchList = [{currentPunch:currentPunch, lastPunchTime:lastPunchTime, punchDate:punchDate,lat:lat, lng:lng}];
                   punchArchive["Punches"] = recentPunchList;
               }
               else {
                   recentPunchList.push({currentPunch:currentPunch, lastPunchTime:lastPunchTime, punchDate:punchDate,lat:lat, lng:lng});
                   punchArchive["Punches"] = recentPunchList;
               }
               dispatch({
                   type:types.ADD_PUNCH,
                   payload: {currentPunch:currentPunch, lastPunchTime:lastPunchTime, punchDate:punchDate,lat:lat, lng:lng, punchArchive:punchArchive}
               });
           }
           else {
               Alert.alert(
                   "Something's wrong!",
                   "Please try again in sometime"
               );
           }
       }).catch((err)=> {
           console.log(err);
           Alert.alert(
               "Something's wrong!",
               `${err}`
           );
       });
   }
   else {
       let recentPunchDate = punchArchive["Date"];
       let recentPunchList = punchArchive["Punches"];
       if(recentPunchDate !== punchDate){
           punchArchive["Date"] = punchDate;
           recentPunchList = [{currentPunch:currentPunch, lastPunchTime:lastPunchTime, punchDate:punchDate,lat:lat, lng:lng}];
           punchArchive["Punches"] = recentPunchList;
       }
       else {
           recentPunchList.push({currentPunch:currentPunch, lastPunchTime:lastPunchTime, punchDate:punchDate,lat:lat, lng:lng});
           punchArchive["Punches"] = recentPunchList;
       }
       dispatch({
           type:types.ADD_PUNCH,
           payload: {currentPunch:currentPunch, lastPunchTime:lastPunchTime, punchDate:punchDate, lat:lat, lng:lng, punchArchive:punchArchive}
       });
       let punchQueue = state.punchData.punchQueue;
       punchQueue.push({
           customerID:userData.customerID,
           companyID:userData.companyID,
           employeeID:userData.employeeID,
           punchType: currentPunch,
           punchDate:punchDate,
           punchTimeStamp: lastPunchTime,
           deviceDetails:userData.imei,
           phoneNumber:userData.phone,
           gpsLongitude: lng,
           gpsLatitude: lat,
           gpsLocation: ""
       });
       dispatch({
           type: types.PUSH_TO_QUEUE,
           payload: punchQueue
       });

       Alert.alert(
           "Added to queue",
           "Please go online to sync your punches"
       );

   }


});

export const loggedIn = (status)=> ((dispatch)=> {
   dispatch({
       type: types.LOGGED_IN,
       payload: status
   });
});

export const syncQueue = ()=> ((dispatch, getState)=> {
    const state = getState();
    let punchQueue = state.punchData.punchQueue;

    if(punchQueue.length > 0){

            fetch('https://worktabapp.fulchrum.com/punch/addpunches', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(punchQueue)
            }).then((response)=> {
                return response.json();
            }).then((data)=> {
                let message = data.message;
                if(message.messageType === "message"){
                    dispatch({
                        type: types.RESET_QUEUE,
                        payload: null
                    });
                }
                else {
                    Alert.alert(
                        "Sync failed!",
                        "Please try again in sometime"
                    );
                }
            }).catch((err)=> {
                console.log(err);
                Alert.alert(
                    "Something's wrong!",
                    `${err}`
                );
            });
    }
});

export const toggleSideMenu = ()=> ((dispatch)=> {
   dispatch({
       type: types.TOGGLE_SIDEMENU,
       payload: null
   });
});

export const connectionStatus = (status) => ((dispatch)=> {
   dispatch({
       type: types.CONNECTION_STATUS,
       payload: status
   });
});

export const showBackButton = (status) => ((dispatch)=> {
    dispatch({
        type: types.SHOW_BACK,
        payload: status
    });
});

export const takeBackAction = (page) => ((dispatch)=> {
    dispatch({
        type: types.TAKE_BACK,
        payload: page
    });
});

export const getStartupData = ()=> ((dispatch, getState)=> {
    const state = getState();
    let {customerID, companyID, employeeID, phone, imei} = state.userData;
    console.log([customerID, companyID, employeeID, phone, imei]);
    let selectedDate = moment().format('YYYY-MM-DD');

    fetch('https://worktabapp.fulchrum.com/startup/startupdata', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            customerID:customerID,
            companyID:companyID,
            employeeID:employeeID,
            phoneNumber:phone,
            deviceDetails:imei,
            selectedDate:selectedDate})
    }).then((response)=> {
        return response.json();
    }).then((data)=> {
        let {gpsInterval,appDateTime,shiftInfo,breaksInfo, message} = data;
        if(message.messageType === "message"){
            // dispatch
            dispatch({
                type:types.STORE_STARTUP_DATA,
                payload: {gpsInterval:gpsInterval, appDateTime:appDateTime, shiftInfo:shiftInfo, breaksInfo:breaksInfo}
            })
        }
        else if (message.messageType === "error"){
            Alert.alert(
                "Something's wrong!",
                `${message.messageString}`
            );
        }

    }).catch((error)=> {
        Alert.alert(
            "Something's wrong",
            `${error}`
        );
    });
});

export const mySchedule = (data)=> ((dispatch, getState)=> {
    const state = getState();
    let customerID = state.userData.customerID;
    let companyID = state.userData.companyID;
    let employeeID = state.userData.employeeID;
    let phone = state.userData.phone;
    let imei = state.userData.imei;
    let fromDate = data.fromDate;
    let toDate = data.toDate;
    let redirect = data.redirect;

    fetch('https://worktabapp.fulchrum.com/shift/empshifts', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            customerID:customerID,
            companyID:companyID,
            employeeID:employeeID,
            phoneNumber:phone,
            deviceDetails:imei,
            fromDate:fromDate,
            toDate:toDate})

    }).then((response)=> {
        return response.json();
    }).then((data)=>{
        let message = data.message;
        if(message.messageType === "message"){
            // dispatch store shift data
            dispatch({
                type: types.STORE_SHIFT_DATA,
                payload: data.employeeShiftList,
            });

            dispatch({
                type: types.LOADING,
                payload: false
            });

            if(redirect){
                Actions.mySchedule();
            }
        }
        else if (message.messageType === "error"){
            dispatch({
                type: types.LOADING,
                payload: false
            });

            Alert.alert(
                "Oops!",
                `${message.messageString}`
            );
        }

    }).catch((err)=> {
        dispatch({
            type: types.LOADING,
            payload: false
        });
        Alert.alert(
            "Something's wrong",
            `${err}`
        )
    });

});

export const registerEmployee = (data) => ((dispatch)=> {
    let {companyName, employeeId, phone, imei, userAgent} = data;

    fetch('https://worktabapp.fulchrum.com/registration/register', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            employeeID:employeeId,
            companyName:companyName,
            phoneNumber:phone,
            deviceDetails:imei,
            deviceDescription:userAgent,
            privateKey:""})

    }).then((response)=> {
        return response.json();
    }).then((data)=> {
        let {companyName, customerID, companyID, departmentName, employeeName,email,managerName,message} = data;
        if(message.messageType === "message"){
            // dispatch registered, store data
            dispatch({
                type: types.REGISTERED,
                payload: true,
            });

            dispatch({
                type:types.STORE_USER_DATA,
                payload:{companyName:companyName, customerID:customerID, companyID:companyID, departmentName:departmentName,
                    employeeName:employeeName,email:email,managerName:managerName,phone:phone, employeeID:employeeId,imei:imei}
            });

            Actions.createPin();




        }
        else if (message.messageType === "error"){
            Alert.alert(
                "Oops!",
                `${message.messageString}`
            );
        }

    }).catch((err)=> {
        console.log(`Error : ${err}`);
        Alert.alert(
            `Something's wrong`,
            `${err}`
        );
    });

});
