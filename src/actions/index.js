import * as WorktabActions from './WorktabActions';

export const ActionCreators = Object.assign({},
    WorktabActions,
);